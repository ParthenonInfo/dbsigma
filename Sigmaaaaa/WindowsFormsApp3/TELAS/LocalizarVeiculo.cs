﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3;
using WindowsFormsApp3.Programação.CadastrarVeiculo;
using WindowsFormsApp3.TELAS;

namespace telas_do_SIGMA
{
    public partial class LocalizarVeiculo : Form
    {
        Validação v = new Validação();
        public LocalizarVeiculo()
        {
            InitializeComponent();
        }

        private void LocalizarVeiculo_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TeladeMenu tela = new TeladeMenu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            VeiculoBusiness business = new VeiculoBusiness();
            List<VeiculoDTO> a = business.Consultar(txtbuscar.Text);
            dgvlocalizar.AutoGenerateColumns = false;
            dgvlocalizar.DataSource = a;

        }

        private void dgvlocalizar_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
