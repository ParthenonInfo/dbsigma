﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.Cliente;
using WindowsFormsApp3.Programação.ClienteJuridico;
using WindowsFormsApp3.Programação.Endereço;
using WindowsFormsApp3.TELAS;

namespace WindowsFormsApp3
{
    public partial class CadastrarClienteJuridico : Form
    {
        Validação v = new Validação();
        public CadastrarClienteJuridico()
        {
            InitializeComponent();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TeladeMenu tela = new TeladeMenu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {

                EndereçoDTO endedto = new EndereçoDTO();

                endedto.rua = txtRuajuridico.Text;
                endedto.bairro = txtbairrojuridico.Text;
                endedto.cidade = txtcidadejuridico.Text;
                endedto.numerocasa = txtnumerojuridico.Text;
                endedto.cep = MtxCEPjuridico.Text;
                endedto.estado = cboEstadojuridico.Text;
                endedto.referencia = txtreferenciajuridico.Text;
                endedto.complemento = txtcomplementojuridico.Text;
                EndereçoBusiness ebusiness = new EndereçoBusiness();
                int a = ebusiness.Salvar(endedto);


                ClienteJuridicoDTO clientedtojuridico = new ClienteJuridicoDTO();


                clientedtojuridico.razaosocialjuridico = txtrazaojuridico.Text;
                clientedtojuridico.cnpjjuridico= MtbCnpjjuridico.Text;
                clientedtojuridico.atividadejuridico= txtatividadejuridico.Text;
                clientedtojuridico.nascimentojuridico= dtpNascimentojuridico.Value;
                clientedtojuridico.clienteobsjuridico= txtobsjuridico.Text;
                clientedtojuridico.telefonejuridico= mtbtelefonejuridico.Text;
                clientedtojuridico.celularjuridico= mtbcelularjuridico.Text;
                clientedtojuridico.telcomercialjuridico= mtbtelcomercialjuridico.Text;
                clientedtojuridico.emailjuridico= txtEmailjuridico.Text;
                clientedtojuridico.ramaljuridico= txtramaljuridico.Text;
                clientedtojuridico.id_endereco = a;

                ClienteJuridicoBusiness business = new ClienteJuridicoBusiness();

                business.Salvar(clientedtojuridico);

                MessageBox.Show("Salvo com sucesso");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);

            }

        }

        private void txtrazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtcnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtprofissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtbairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtcidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtncasa_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtcep_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void cboEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtreferencia_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtTelComercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void maskedTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void dtpNascimento_ValueChanged(object sender, EventArgs e)
        {

        }

        private void CadastrarClienteJuridico_Load(object sender, EventArgs e)
        {

        }

        private void txtramal_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtramal_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtcomplemento_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
