﻿namespace WindowsFormsApp3
{
    partial class CadastrarClienteGerais
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastrarClienteGerais));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtobs = new System.Windows.Forms.TextBox();
            this.mtbtelcomercial = new System.Windows.Forms.MaskedTextBox();
            this.lblcliente = new System.Windows.Forms.Label();
            this.mtbcelular = new System.Windows.Forms.MaskedTextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.mtbtelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.mtbCep = new System.Windows.Forms.MaskedTextBox();
            this.lblProfissao = new System.Windows.Forms.Label();
            this.MtxtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtProfissao = new System.Windows.Forms.TextBox();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblBataNasc = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblRamal = new System.Windows.Forms.Label();
            this.lblReferencia = new System.Windows.Forms.Label();
            this.txtramal = new System.Windows.Forms.TextBox();
            this.txtreferencia = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblCEP = new System.Windows.Forms.Label();
            this.lblCelular = new System.Windows.Forms.Label();
            this.txtncasa = new System.Windows.Forms.TextBox();
            this.lblTelComercial = new System.Windows.Forms.Label();
            this.lblComplemento = new System.Windows.Forms.Label();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.lblContato = new System.Windows.Forms.Label();
            this.lblRua = new System.Windows.Forms.Label();
            this.cboEstado = new System.Windows.Forms.ComboBox();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblBairro = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::WindowsFormsApp3.Properties.Resources.IMG_20180826_WA0004;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(751, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 43);
            this.pictureBox4.TabIndex = 93;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::WindowsFormsApp3.Properties.Resources.IMG_20180826_WA0003;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(723, 403);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(81, 67);
            this.pictureBox1.TabIndex = 90;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 349);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 16);
            this.label3.TabIndex = 96;
            this.label3.Text = "Informações adicionais:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtobs
            // 
            this.txtobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtobs.Location = new System.Drawing.Point(18, 368);
            this.txtobs.MaxLength = 50;
            this.txtobs.Multiline = true;
            this.txtobs.Name = "txtobs";
            this.txtobs.Size = new System.Drawing.Size(673, 102);
            this.txtobs.TabIndex = 18;
            // 
            // mtbtelcomercial
            // 
            this.mtbtelcomercial.Location = new System.Drawing.Point(588, 160);
            this.mtbtelcomercial.Mask = "(00) 0000-0000";
            this.mtbtelcomercial.Name = "mtbtelcomercial";
            this.mtbtelcomercial.Size = new System.Drawing.Size(100, 20);
            this.mtbtelcomercial.TabIndex = 15;
            this.mtbtelcomercial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbtelcomercial_KeyPress);
            // 
            // lblcliente
            // 
            this.lblcliente.AutoSize = true;
            this.lblcliente.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcliente.Location = new System.Drawing.Point(106, 38);
            this.lblcliente.Name = "lblcliente";
            this.lblcliente.Size = new System.Drawing.Size(53, 18);
            this.lblcliente.TabIndex = 61;
            this.lblcliente.Text = "Cliente";
            // 
            // mtbcelular
            // 
            this.mtbcelular.Location = new System.Drawing.Point(588, 122);
            this.mtbcelular.Mask = "(00) 00000-0000";
            this.mtbcelular.Name = "mtbcelular";
            this.mtbcelular.Size = new System.Drawing.Size(100, 20);
            this.mtbcelular.TabIndex = 14;
            this.mtbcelular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbcelular_KeyPress);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(53, 81);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(50, 18);
            this.lblNome.TabIndex = 46;
            this.lblNome.Text = "Nome:";
            // 
            // mtbtelefone
            // 
            this.mtbtelefone.Location = new System.Drawing.Point(588, 88);
            this.mtbtelefone.Mask = "(00) 0000-0000";
            this.mtbtelefone.Name = "mtbtelefone";
            this.mtbtelefone.Size = new System.Drawing.Size(100, 20);
            this.mtbtelefone.TabIndex = 13;
            this.mtbtelefone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mtbtelefone_MaskInputRejected);
            this.mtbtelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbtelefone_KeyPress);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(109, 84);
            this.txtNome.MaxLength = 20;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(100, 20);
            this.txtNome.TabIndex = 1;
            this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // mtbCep
            // 
            this.mtbCep.Location = new System.Drawing.Point(343, 224);
            this.mtbCep.Mask = "00000-000";
            this.mtbCep.Name = "mtbCep";
            this.mtbCep.Size = new System.Drawing.Size(124, 20);
            this.mtbCep.TabIndex = 9;
            this.mtbCep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbCep_KeyPress);
            // 
            // lblProfissao
            // 
            this.lblProfissao.AutoSize = true;
            this.lblProfissao.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfissao.Location = new System.Drawing.Point(25, 150);
            this.lblProfissao.Name = "lblProfissao";
            this.lblProfissao.Size = new System.Drawing.Size(70, 18);
            this.lblProfissao.TabIndex = 48;
            this.lblProfissao.Text = "Profissão:";
            // 
            // MtxtCPF
            // 
            this.MtxtCPF.Location = new System.Drawing.Point(109, 116);
            this.MtxtCPF.Mask = "000.000.000-00";
            this.MtxtCPF.Name = "MtxtCPF";
            this.MtxtCPF.Size = new System.Drawing.Size(100, 20);
            this.MtxtCPF.TabIndex = 2;
            this.MtxtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox1_KeyPress);
            // 
            // txtProfissao
            // 
            this.txtProfissao.Location = new System.Drawing.Point(109, 150);
            this.txtProfissao.MaxLength = 40;
            this.txtProfissao.Name = "txtProfissao";
            this.txtProfissao.Size = new System.Drawing.Size(100, 20);
            this.txtProfissao.TabIndex = 3;
            this.txtProfissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProfissao_KeyPress);
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(62, 113);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(35, 18);
            this.lblCPF.TabIndex = 50;
            this.lblCPF.Text = "CPF:";
            // 
            // lblBataNasc
            // 
            this.lblBataNasc.AutoSize = true;
            this.lblBataNasc.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBataNasc.Location = new System.Drawing.Point(15, 171);
            this.lblBataNasc.Name = "lblBataNasc";
            this.lblBataNasc.Size = new System.Drawing.Size(85, 36);
            this.lblBataNasc.TabIndex = 52;
            this.lblBataNasc.Text = "Data de \r\nnascimento:";
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.CustomFormat = "dd-mm-yyyy";
            this.dtpNascimento.Location = new System.Drawing.Point(109, 182);
            this.dtpNascimento.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpNascimento.MinDate = new System.DateTime(1900, 12, 31, 0, 0, 0, 0);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(100, 20);
            this.dtpNascimento.TabIndex = 4;
            this.dtpNascimento.Value = new System.DateTime(2019, 12, 31, 0, 0, 0, 0);
            this.dtpNascimento.ValueChanged += new System.EventHandler(this.dtpNascimento_ValueChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(588, 202);
            this.txtEmail.MaxLength = 20;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEmail.TabIndex = 16;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(225, 189);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(112, 18);
            this.lblNumero.TabIndex = 62;
            this.lblNumero.Text = "Número da casa:";
            // 
            // lblRamal
            // 
            this.lblRamal.AutoSize = true;
            this.lblRamal.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRamal.Location = new System.Drawing.Point(526, 235);
            this.lblRamal.Name = "lblRamal";
            this.lblRamal.Size = new System.Drawing.Size(51, 18);
            this.lblRamal.TabIndex = 88;
            this.lblRamal.Text = "Ramal:";
            // 
            // lblReferencia
            // 
            this.lblReferencia.AutoSize = true;
            this.lblReferencia.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferencia.Location = new System.Drawing.Point(251, 282);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(82, 18);
            this.lblReferencia.TabIndex = 64;
            this.lblReferencia.Text = "Referência:";
            // 
            // txtramal
            // 
            this.txtramal.Location = new System.Drawing.Point(588, 242);
            this.txtramal.MaxLength = 5;
            this.txtramal.Name = "txtramal";
            this.txtramal.Size = new System.Drawing.Size(100, 20);
            this.txtramal.TabIndex = 17;
            this.txtramal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtramal_KeyPress);
            // 
            // txtreferencia
            // 
            this.txtreferencia.Location = new System.Drawing.Point(343, 282);
            this.txtreferencia.MaxLength = 40;
            this.txtreferencia.Name = "txtreferencia";
            this.txtreferencia.Size = new System.Drawing.Size(124, 20);
            this.txtreferencia.TabIndex = 11;
            this.txtreferencia.TextChanged += new System.EventHandler(this.txtreferencia_TextChanged);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(529, 202);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(47, 18);
            this.lblEmail.TabIndex = 86;
            this.lblEmail.Text = "Email:";
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(298, 226);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(36, 18);
            this.lblCEP.TabIndex = 66;
            this.lblCEP.Text = "CEP:";
            this.lblCEP.Click += new System.EventHandler(this.lblCEP_Click);
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.Location = new System.Drawing.Point(521, 123);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(57, 18);
            this.lblCelular.TabIndex = 85;
            this.lblCelular.Text = "Celular:";
            // 
            // txtncasa
            // 
            this.txtncasa.Location = new System.Drawing.Point(343, 191);
            this.txtncasa.MaxLength = 5;
            this.txtncasa.Name = "txtncasa";
            this.txtncasa.Size = new System.Drawing.Size(124, 20);
            this.txtncasa.TabIndex = 8;
            this.txtncasa.TextChanged += new System.EventHandler(this.txtncasa_TextChanged);
            this.txtncasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtncasa_KeyPress);
            // 
            // lblTelComercial
            // 
            this.lblTelComercial.AutoSize = true;
            this.lblTelComercial.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelComercial.Location = new System.Drawing.Point(498, 150);
            this.lblTelComercial.Name = "lblTelComercial";
            this.lblTelComercial.Size = new System.Drawing.Size(76, 36);
            this.lblTelComercial.TabIndex = 84;
            this.lblTelComercial.Text = "Telefone \r\nComercial:";
            // 
            // lblComplemento
            // 
            this.lblComplemento.AutoSize = true;
            this.lblComplemento.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(232, 310);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.Size = new System.Drawing.Size(101, 18);
            this.lblComplemento.TabIndex = 68;
            this.lblComplemento.Text = "Complemento:";
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(514, 88);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(68, 18);
            this.lblTelefone.TabIndex = 83;
            this.lblTelefone.Text = "Telefone:";
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(343, 306);
            this.txtcomplemento.MaxLength = 10;
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(124, 20);
            this.txtcomplemento.TabIndex = 12;
            this.txtcomplemento.TextChanged += new System.EventHandler(this.txtcomplemento_TextChanged);
            // 
            // lblContato
            // 
            this.lblContato.AutoSize = true;
            this.lblContato.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContato.Location = new System.Drawing.Point(585, 40);
            this.lblContato.Name = "lblContato";
            this.lblContato.Size = new System.Drawing.Size(61, 18);
            this.lblContato.TabIndex = 82;
            this.lblContato.Text = "Contato:";
            // 
            // lblRua
            // 
            this.lblRua.AutoSize = true;
            this.lblRua.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRua.Location = new System.Drawing.Point(300, 85);
            this.lblRua.Name = "lblRua";
            this.lblRua.Size = new System.Drawing.Size(36, 18);
            this.lblRua.TabIndex = 70;
            this.lblRua.Text = "Rua:";
            // 
            // cboEstado
            // 
            this.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEstado.FormattingEnabled = true;
            this.cboEstado.Items.AddRange(new object[] {
            "Acre",
            "Amapá",
            "Amazonas",
            "Pará",
            "Rondônia",
            "Roraima ",
            "Tocantins",
            "Alagoas",
            "Bahia",
            "Ceará",
            "Maranhão",
            "Paraíba",
            "Pernambuco",
            "Piauí",
            "Rio Grande do Norte",
            "Sergipe",
            "Goiás",
            "Mato Grosso",
            "Mato Grosso do Sul ",
            "Distrito Federal",
            "Espírito Santo",
            "Minas Gerais",
            "São Paulo",
            "Rio de Janeiro",
            "Paraná",
            "Rio Grande do Sul",
            "Santa Catarina"});
            this.cboEstado.Location = new System.Drawing.Point(343, 252);
            this.cboEstado.Name = "cboEstado";
            this.cboEstado.Size = new System.Drawing.Size(124, 21);
            this.cboEstado.TabIndex = 10;
            this.cboEstado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEstado_KeyPress);
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(343, 84);
            this.txtRua.MaxLength = 40;
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(124, 20);
            this.txtRua.TabIndex = 5;
            this.txtRua.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRua_KeyPress);
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(278, 253);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(54, 18);
            this.lblEstado.TabIndex = 77;
            this.lblEstado.Text = "Estado:";
            // 
            // lblBairro
            // 
            this.lblBairro.AutoSize = true;
            this.lblBairro.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(286, 121);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(52, 18);
            this.lblBairro.TabIndex = 72;
            this.lblBairro.Text = "Bairro:";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(362, 38);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(69, 18);
            this.lblEndereco.TabIndex = 76;
            this.lblEndereco.Text = "Endereço";
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(343, 120);
            this.txtbairro.MaxLength = 40;
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(124, 20);
            this.txtbairro.TabIndex = 6;
            this.txtbairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbairro_KeyPress);
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(343, 154);
            this.txtcidade.MaxLength = 10;
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(124, 20);
            this.txtcidade.TabIndex = 7;
            this.txtcidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcidade_KeyPress);
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(278, 154);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(55, 18);
            this.lblCidade.TabIndex = 74;
            this.lblCidade.Text = "Cidade:";
            // 
            // CadastrarClienteGerais
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 482);
            this.Controls.Add(this.mtbtelcomercial);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblcliente);
            this.Controls.Add(this.mtbcelular);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.txtobs);
            this.Controls.Add(this.mtbtelefone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.mtbCep);
            this.Controls.Add(this.lblCidade);
            this.Controls.Add(this.lblProfissao);
            this.Controls.Add(this.txtcidade);
            this.Controls.Add(this.MtxtCPF);
            this.Controls.Add(this.txtbairro);
            this.Controls.Add(this.txtProfissao);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.lblBairro);
            this.Controls.Add(this.lblBataNasc);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.dtpNascimento);
            this.Controls.Add(this.txtRua);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.cboEstado);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.lblRua);
            this.Controls.Add(this.lblRamal);
            this.Controls.Add(this.lblContato);
            this.Controls.Add(this.lblReferencia);
            this.Controls.Add(this.txtcomplemento);
            this.Controls.Add(this.txtramal);
            this.Controls.Add(this.lblTelefone);
            this.Controls.Add(this.txtreferencia);
            this.Controls.Add(this.lblComplemento);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblTelComercial);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.txtncasa);
            this.Controls.Add(this.lblCelular);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastrarClienteGerais";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastrarClienteGerais";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtobs;
        private System.Windows.Forms.MaskedTextBox mtbtelcomercial;
        private System.Windows.Forms.Label lblcliente;
        private System.Windows.Forms.MaskedTextBox mtbcelular;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.MaskedTextBox mtbtelefone;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.MaskedTextBox mtbCep;
        private System.Windows.Forms.Label lblProfissao;
        private System.Windows.Forms.MaskedTextBox MtxtCPF;
        private System.Windows.Forms.TextBox txtProfissao;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblBataNasc;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblRamal;
        private System.Windows.Forms.Label lblReferencia;
        private System.Windows.Forms.TextBox txtramal;
        private System.Windows.Forms.TextBox txtreferencia;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.TextBox txtncasa;
        private System.Windows.Forms.Label lblTelComercial;
        private System.Windows.Forms.Label lblComplemento;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.Label lblContato;
        private System.Windows.Forms.Label lblRua;
        private System.Windows.Forms.ComboBox cboEstado;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.Label lblCidade;
    }
}