﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using telas_do_SIGMA;
using WindowsFormsApp3.Programação.Serviço;

namespace WindowsFormsApp3.TELAS
{
    public partial class CadastrarServico : Form
    {
        Validação v = new Validação();
        public CadastrarServico()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                ServiçoDTO servico = new ServiçoDTO();
                servico.nomeservico = txtnome.Text;
                servico.preco = Convert.ToDecimal(txtpreco.Text);
                servico.descricao = txtdescricao.Text;

                ServiçoBusiness b = new ServiçoBusiness();
                b.Salvar(servico);
                MessageBox.Show("Serviço Salvo");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Orçamento tela = new Orçamento();
            tela.Show();
            this.Hide();



        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
