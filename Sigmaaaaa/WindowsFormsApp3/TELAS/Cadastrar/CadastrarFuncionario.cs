﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.Cliente;
using WindowsFormsApp3.Programação.Departamento;
using WindowsFormsApp3.Programação.Endereço;
using WindowsFormsApp3.TELAS;

namespace WindowsFormsApp3
{
    public partial class CadastrarFuncionario : Form
    {
        Validação v = new Validação();
        public CadastrarFuncionario()
        {
            InitializeComponent();
            CarregarCombos();
        }


        void CarregarCombos()
        {
            DepartamentoBusiness bus = new DepartamentoBusiness();
            List<DepartamentoDTO> lista = bus.Listar();
            cmbdepartamento.ValueMember = nameof(DepartamentoDTO.Id);
            cmbdepartamento.DisplayMember = nameof(DepartamentoDTO.Nome);
            cmbdepartamento.DataSource = lista;
        }

        private void lblEndereco_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TeladeMenu tela = new TeladeMenu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
      
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            try
            {
                EndereçoDTO endedto = new EndereçoDTO();

                endedto.rua = txtRua.Text;
                endedto.bairro = txtbairro.Text;
                endedto.cidade = txtcidade.Text;
                endedto.cep = MtbCep.Text;
                endedto.estado = cboEstado.Text;
                endedto.referencia = txtreferencia.Text;
                endedto.complemento = txtcomplemento.Text;
                endedto.numerocasa = txtncasa.Text;
                EndereçoBusiness ebusiness = new EndereçoBusiness();
                int a = ebusiness.Salvar(endedto);



                DepartamentoDTO b = cmbdepartamento.SelectedItem as DepartamentoDTO; 

                FuncionarioDTO funcionariodto = new FuncionarioDTO();
                funcionariodto.nome = txtNome.Text;
                funcionariodto.cpf = MtxCPFF.Text;
                funcionariodto.rg = mtbRg.Text;
                funcionariodto.telefone = mtbtelefonefuncionario.Text;
                funcionariodto.celular = mtbcelularfuncionario.Text;
                funcionariodto.email = txtemail.Text;
                funcionariodto.idendereco = a;
                funcionariodto.nascimento = dtpnascimento.Value;
                funcionariodto.iddepartamento = b.Id;
                funcionariodto.usuario = txtusuario.Text;
                funcionariodto.senha = txtsenha.Text;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(funcionariodto);
                MessageBox.Show("Salvo");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);

            }

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtcpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtrg_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void cmbdepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtbairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtcidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void cboEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtncasa_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtcep_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txttelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtcelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtemail_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void dtpnascimento_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
