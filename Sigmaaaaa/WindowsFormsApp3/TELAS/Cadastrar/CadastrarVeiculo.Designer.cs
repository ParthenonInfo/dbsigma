﻿namespace WindowsFormsApp3
{
    partial class CadastrarVeiculo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastrarVeiculo));
            this.txtCor = new System.Windows.Forms.TextBox();
            this.cboCombustivel = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtinformacao = new System.Windows.Forms.TextBox();
            this.txtproprietario = new System.Windows.Forms.TextBox();
            this.txtmodelo = new System.Windows.Forms.TextBox();
            this.TxtOdometro = new System.Windows.Forms.TextBox();
            this.TxtAno = new System.Windows.Forms.TextBox();
            this.mtbPlaca = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCor
            // 
            this.txtCor.Location = new System.Drawing.Point(343, 74);
            this.txtCor.MaxLength = 18;
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(121, 20);
            this.txtCor.TabIndex = 6;
            this.txtCor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCor_KeyPress);
            // 
            // cboCombustivel
            // 
            this.cboCombustivel.FormattingEnabled = true;
            this.cboCombustivel.Items.AddRange(new object[] {
            "Gasolina comum",
            "Gasolina aditivada",
            "Etanol",
            "Diesel",
            "Gás Natural Veicular (GNV)",
            "Flex"});
            this.cboCombustivel.Location = new System.Drawing.Point(343, 21);
            this.cboCombustivel.Name = "cboCombustivel";
            this.cboCombustivel.Size = new System.Drawing.Size(121, 21);
            this.cboCombustivel.TabIndex = 4;
            this.cboCombustivel.Text = "Gasolina Comum";
            this.cboCombustivel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCombustivel_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(301, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 18);
            this.label8.TabIndex = 28;
            this.label8.Text = "Ano:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(304, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 18);
            this.label9.TabIndex = 27;
            this.label9.Text = "Cor:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(260, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 18);
            this.label7.TabIndex = 26;
            this.label7.Text = "Odômetro:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(248, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 18);
            this.label6.TabIndex = 25;
            this.label6.Text = "Combustível:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 18);
            this.label4.TabIndex = 22;
            this.label4.Text = "Modelo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "Proprietário: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(50, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 19;
            this.label2.Text = "Placa:";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::WindowsFormsApp3.Properties.Resources.IMG_20180826_WA0004;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(588, 7);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 43);
            this.pictureBox4.TabIndex = 97;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::WindowsFormsApp3.Properties.Resources.IMG_20180826_WA0003;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(551, 237);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(81, 67);
            this.pictureBox1.TabIndex = 94;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 16);
            this.label10.TabIndex = 98;
            this.label10.Text = "Informações Adicionais";
            // 
            // txtinformacao
            // 
            this.txtinformacao.Location = new System.Drawing.Point(8, 154);
            this.txtinformacao.MaxLength = 100;
            this.txtinformacao.Multiline = true;
            this.txtinformacao.Name = "txtinformacao";
            this.txtinformacao.Size = new System.Drawing.Size(519, 149);
            this.txtinformacao.TabIndex = 8;
            // 
            // txtproprietario
            // 
            this.txtproprietario.Location = new System.Drawing.Point(109, 56);
            this.txtproprietario.MaxLength = 15;
            this.txtproprietario.Name = "txtproprietario";
            this.txtproprietario.Size = new System.Drawing.Size(100, 20);
            this.txtproprietario.TabIndex = 2;
            this.txtproprietario.TextChanged += new System.EventHandler(this.txtproprietario_TextChanged);
            this.txtproprietario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtproprietario_KeyPress);
            // 
            // txtmodelo
            // 
            this.txtmodelo.Location = new System.Drawing.Point(109, 82);
            this.txtmodelo.MaxLength = 10;
            this.txtmodelo.Name = "txtmodelo";
            this.txtmodelo.Size = new System.Drawing.Size(100, 20);
            this.txtmodelo.TabIndex = 3;
            this.txtmodelo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmodelo_KeyPress);
            // 
            // TxtOdometro
            // 
            this.TxtOdometro.Location = new System.Drawing.Point(343, 48);
            this.TxtOdometro.MaxLength = 8;
            this.TxtOdometro.Name = "TxtOdometro";
            this.TxtOdometro.Size = new System.Drawing.Size(121, 20);
            this.TxtOdometro.TabIndex = 5;
            this.TxtOdometro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // TxtAno
            // 
            this.TxtAno.Location = new System.Drawing.Point(343, 99);
            this.TxtAno.MaxLength = 4;
            this.TxtAno.Name = "TxtAno";
            this.TxtAno.Size = new System.Drawing.Size(121, 20);
            this.TxtAno.TabIndex = 7;
            this.TxtAno.TextChanged += new System.EventHandler(this.TxtAno_TextChanged);
            this.TxtAno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtAno_KeyPress);
            // 
            // mtbPlaca
            // 
            this.mtbPlaca.Location = new System.Drawing.Point(109, 30);
            this.mtbPlaca.Mask = "AAA-0000";
            this.mtbPlaca.Name = "mtbPlaca";
            this.mtbPlaca.Size = new System.Drawing.Size(100, 20);
            this.mtbPlaca.TabIndex = 99;
            // 
            // CadastrarVeiculo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 316);
            this.Controls.Add(this.mtbPlaca);
            this.Controls.Add(this.TxtAno);
            this.Controls.Add(this.TxtOdometro);
            this.Controls.Add(this.txtmodelo);
            this.Controls.Add(this.txtproprietario);
            this.Controls.Add(this.txtinformacao);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtCor);
            this.Controls.Add(this.cboCombustivel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastrarVeiculo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastrarVeiculo";
            this.Load += new System.EventHandler(this.CadastrarVeiculo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.ComboBox cboCombustivel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtinformacao;
        private System.Windows.Forms.TextBox txtproprietario;
        private System.Windows.Forms.TextBox txtmodelo;
        private System.Windows.Forms.TextBox TxtOdometro;
        private System.Windows.Forms.TextBox TxtAno;
        private System.Windows.Forms.MaskedTextBox mtbPlaca;
    }
}