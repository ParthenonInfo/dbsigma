﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.CadastrarVeiculo;
using WindowsFormsApp3.TELAS;

namespace WindowsFormsApp3
{
    public partial class CadastrarVeiculo : Form
    {
        Validação v = new Validação();
        public CadastrarVeiculo()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TeladeMenu tela = new TeladeMenu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {

                VeiculoDTO veiculodto = new VeiculoDTO();
                veiculodto.placa = mtbPlaca.Text;
                veiculodto.combustivel = cboCombustivel.Text;
                veiculodto.odometro = TxtOdometro.Text;
                veiculodto.cor = txtCor.Text;
                veiculodto.ano = TxtAno.Text;
                veiculodto.proprietario = txtproprietario.Text;
                veiculodto.modelo = txtmodelo.Text;
                veiculodto.Informação = txtinformacao.Text;


                VeiculoBusiness business = new VeiculoBusiness();
                business.Salvar(veiculodto);
                MessageBox.Show("Veículo salvo com sucesso");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }


        }

        private void txtproprietario_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void cboCombustivel_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }
     
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtCor_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void TxtAno_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtproprietario_TextChanged(object sender, EventArgs e)
        {

        }

        private void CadastrarVeiculo_Load(object sender, EventArgs e)
        {

        }

        private void TxtAno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtmodelo_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }
    }
}
