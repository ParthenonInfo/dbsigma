﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.Cliente;
using WindowsFormsApp3.Programação.Endereço;
using WindowsFormsApp3.TELAS;

namespace WindowsFormsApp3
{
    public partial class CadastrarClienteGerais : Form
    {
        Validação v = new Validação();
        public CadastrarClienteGerais()
        {
            InitializeComponent();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TeladeMenu tela = new TeladeMenu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            { 

                    EndereçoDTO endedto = new EndereçoDTO();


                    endedto.rua = txtRua.Text;
                    endedto.bairro = txtbairro.Text;
                    endedto.cidade = txtcidade.Text;
                    endedto.numerocasa = txtncasa.Text;
                    endedto.cep = mtbCep.Text;
                    endedto.estado = cboEstado.Text;
                    endedto.referencia = txtreferencia.Text;
                    endedto.complemento = txtcomplemento.Text;


                    EndereçoBusiness ebusiness = new EndereçoBusiness();


                    int enderco = ebusiness.Salvar(endedto);



                    ClienteDTO clientedto = new ClienteDTO();

                    clientedto.Nome = txtNome.Text;
                    clientedto.cpf = MtxtCPF.Text;
                    clientedto.profissao = txtProfissao.Text;
                    clientedto.nascimento = dtpNascimento.Value;
                    clientedto.telefone = mtbtelefone.Text;
                    clientedto.celular = mtbcelular.Text;
                    clientedto.telcomercial = mtbtelcomercial.Text;
                    clientedto.email = txtEmail.Text;
                    clientedto.ramal = txtramal.Text;
                    clientedto.Id_Endereco = enderco;



                    ClienteBusiness business = new ClienteBusiness();

                    business.Salvar(clientedto);
                    MessageBox.Show("Salvo com sucesso");
                

            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro:" + ex.Message);
            }

        }

        private void lblCEP_Click(object sender, EventArgs e)
        {
            
        }

        private void LblCEP_Click(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }


        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtProfissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtbairro_KeyPress(object sender, KeyPressEventArgs e)
        {

            v.letras(e);
        }

        private void txtcidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtreferencia_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtcomplemento_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtCPF_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtCPF_KeyPress(object sender, EventArgs e)
        {
         
        }

        private void txtncasa_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void txtcep_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtTelefone_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtCelular_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtTelComercial_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void txtncasa_KeyPress(object sender, KeyPressEventArgs e)
        {

            v.numeros(e);

        }

        private void txtcep_KeyPress(object sender, KeyPressEventArgs e)
        {

            v.numeros(e);
            

        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtTelComercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void cboEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void mtbCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void dtpNascimento_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void txtusuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtramal_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void mtbtelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            
        }

        private void mtbtelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void mtbcelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void mtbtelcomercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvarJuridico_Click(object sender, EventArgs e)
        {
           
        }

        private void txtrazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void MtbCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void textBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtruaA_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtbairroA_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtnumeroA_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void MtxCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void textBox19_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtTelefone_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtCelular_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtTelComercial_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtem_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtrazaosoc_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
