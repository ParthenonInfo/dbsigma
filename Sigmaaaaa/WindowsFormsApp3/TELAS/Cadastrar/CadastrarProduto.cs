﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using telas_do_SIGMA;
using WindowsFormsApp3.Programação.Produto;

namespace WindowsFormsApp3.TELAS
{
    public partial class CadastrarProduto : Form
    {
        Validação v = new Validação();
        public CadastrarProduto()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            { 
            ProdutoDTO produto = new ProdutoDTO();
            produto.nomeproduto = txtnome.Text;
            produto.preco = Convert.ToDecimal(txtpreco.Text);
            produto.descricao = txtdescricao.Text;

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(produto);

            MessageBox.Show("Produto salvo");
            }    
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Orçamento tela = new Orçamento();
            tela.Show();
            this.Hide();
        }

        private void CadastrarProduto_Load(object sender, EventArgs e)
        {

        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
