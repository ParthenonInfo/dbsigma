﻿namespace WindowsFormsApp3
{
    partial class CadastrarClienteJuridico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastrarClienteJuridico));
            this.txtEmailjuridico = new System.Windows.Forms.TextBox();
            this.lblRamal = new System.Windows.Forms.Label();
            this.txtramaljuridico = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblCelular = new System.Windows.Forms.Label();
            this.lblTelComercial = new System.Windows.Forms.Label();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.lblContato = new System.Windows.Forms.Label();
            this.cboEstadojuridico = new System.Windows.Forms.ComboBox();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.txtcidadejuridico = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.txtbairrojuridico = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.Label();
            this.txtRuajuridico = new System.Windows.Forms.TextBox();
            this.lblRua = new System.Windows.Forms.Label();
            this.txtcomplementojuridico = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.Label();
            this.txtnumerojuridico = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.txtreferenciajuridico = new System.Windows.Forms.TextBox();
            this.lblReferencia = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.dtpNascimentojuridico = new System.Windows.Forms.DateTimePicker();
            this.lblBataNasc = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.txtatividadejuridico = new System.Windows.Forms.TextBox();
            this.lblProfissao = new System.Windows.Forms.Label();
            this.txtrazaojuridico = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.txtobsjuridico = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.MtxCEPjuridico = new System.Windows.Forms.MaskedTextBox();
            this.MtbCnpjjuridico = new System.Windows.Forms.MaskedTextBox();
            this.mtbtelefonejuridico = new System.Windows.Forms.MaskedTextBox();
            this.mtbtelcomercialjuridico = new System.Windows.Forms.MaskedTextBox();
            this.mtbcelularjuridico = new System.Windows.Forms.MaskedTextBox();
            this.btnSalvarJuridico = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalvarJuridico)).BeginInit();
            this.SuspendLayout();
            // 
            // txtEmailjuridico
            // 
            this.txtEmailjuridico.Location = new System.Drawing.Point(547, 223);
            this.txtEmailjuridico.MaxLength = 20;
            this.txtEmailjuridico.Name = "txtEmailjuridico";
            this.txtEmailjuridico.Size = new System.Drawing.Size(100, 20);
            this.txtEmailjuridico.TabIndex = 16;
            this.txtEmailjuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail_KeyPress);
            // 
            // lblRamal
            // 
            this.lblRamal.AutoSize = true;
            this.lblRamal.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRamal.Location = new System.Drawing.Point(484, 261);
            this.lblRamal.Name = "lblRamal";
            this.lblRamal.Size = new System.Drawing.Size(51, 18);
            this.lblRamal.TabIndex = 116;
            this.lblRamal.Text = "Ramal:";
            // 
            // txtramaljuridico
            // 
            this.txtramaljuridico.Location = new System.Drawing.Point(547, 263);
            this.txtramaljuridico.MaxLength = 5;
            this.txtramaljuridico.Name = "txtramaljuridico";
            this.txtramaljuridico.Size = new System.Drawing.Size(100, 20);
            this.txtramaljuridico.TabIndex = 17;
            this.txtramaljuridico.TextChanged += new System.EventHandler(this.txtramal_TextChanged);
            this.txtramaljuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtramal_KeyPress);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(490, 220);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(47, 18);
            this.lblEmail.TabIndex = 114;
            this.lblEmail.Text = "Email:";
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.Location = new System.Drawing.Point(474, 144);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(57, 18);
            this.lblCelular.TabIndex = 113;
            this.lblCelular.Text = "Celular:";
            // 
            // lblTelComercial
            // 
            this.lblTelComercial.AutoSize = true;
            this.lblTelComercial.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelComercial.Location = new System.Drawing.Point(459, 173);
            this.lblTelComercial.Name = "lblTelComercial";
            this.lblTelComercial.Size = new System.Drawing.Size(76, 36);
            this.lblTelComercial.TabIndex = 112;
            this.lblTelComercial.Text = "Telefone \r\nComercial:";
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(467, 109);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(68, 18);
            this.lblTelefone.TabIndex = 111;
            this.lblTelefone.Text = "Telefone:";
            // 
            // lblContato
            // 
            this.lblContato.AutoSize = true;
            this.lblContato.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContato.Location = new System.Drawing.Point(544, 69);
            this.lblContato.Name = "lblContato";
            this.lblContato.Size = new System.Drawing.Size(61, 18);
            this.lblContato.TabIndex = 110;
            this.lblContato.Text = "Contato:";
            // 
            // cboEstadojuridico
            // 
            this.cboEstadojuridico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEstadojuridico.FormattingEnabled = true;
            this.cboEstadojuridico.Items.AddRange(new object[] {
            "Acre",
            "Amapá",
            "Amazonas",
            "Pará",
            "Rondônia",
            "Roraima ",
            "Tocantins",
            "Alagoas",
            "Bahia",
            "Ceará",
            "Maranhão",
            "Paraíba",
            "Pernambuco",
            "Piauí",
            "Rio Grande do Norte",
            "Sergipe",
            "Goiás",
            "Mato Grosso",
            "Mato Grosso do Sul ",
            "Distrito Federal",
            "Espírito Santo",
            "Minas Gerais",
            "São Paulo",
            "Rio de Janeiro",
            "Paraná",
            "Rio Grande do Sul",
            "Santa Catarina"});
            this.cboEstadojuridico.Location = new System.Drawing.Point(343, 300);
            this.cboEstadojuridico.Name = "cboEstadojuridico";
            this.cboEstadojuridico.Size = new System.Drawing.Size(100, 21);
            this.cboEstadojuridico.TabIndex = 10;
            this.cboEstadojuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEstado_KeyPress);
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(276, 301);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(54, 18);
            this.lblEstado.TabIndex = 105;
            this.lblEstado.Text = "Estado:";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(340, 69);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(69, 18);
            this.lblEndereco.TabIndex = 104;
            this.lblEndereco.Text = "Endereço";
            // 
            // txtcidadejuridico
            // 
            this.txtcidadejuridico.Location = new System.Drawing.Point(342, 187);
            this.txtcidadejuridico.MaxLength = 10;
            this.txtcidadejuridico.Name = "txtcidadejuridico";
            this.txtcidadejuridico.Size = new System.Drawing.Size(100, 20);
            this.txtcidadejuridico.TabIndex = 7;
            this.txtcidadejuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcidade_KeyPress);
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(277, 184);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(55, 18);
            this.lblCidade.TabIndex = 102;
            this.lblCidade.Text = "Cidade:";
            // 
            // txtbairrojuridico
            // 
            this.txtbairrojuridico.Location = new System.Drawing.Point(343, 149);
            this.txtbairrojuridico.MaxLength = 20;
            this.txtbairrojuridico.Name = "txtbairrojuridico";
            this.txtbairrojuridico.Size = new System.Drawing.Size(100, 20);
            this.txtbairrojuridico.TabIndex = 6;
            this.txtbairrojuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbairro_KeyPress);
            // 
            // lblBairro
            // 
            this.lblBairro.AutoSize = true;
            this.lblBairro.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.Location = new System.Drawing.Point(282, 149);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(52, 18);
            this.lblBairro.TabIndex = 100;
            this.lblBairro.Text = "Bairro:";
            // 
            // txtRuajuridico
            // 
            this.txtRuajuridico.Location = new System.Drawing.Point(343, 112);
            this.txtRuajuridico.MaxLength = 20;
            this.txtRuajuridico.Name = "txtRuajuridico";
            this.txtRuajuridico.Size = new System.Drawing.Size(100, 20);
            this.txtRuajuridico.TabIndex = 5;
            this.txtRuajuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRua_KeyPress);
            // 
            // lblRua
            // 
            this.lblRua.AutoSize = true;
            this.lblRua.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRua.Location = new System.Drawing.Point(291, 109);
            this.lblRua.Name = "lblRua";
            this.lblRua.Size = new System.Drawing.Size(36, 18);
            this.lblRua.TabIndex = 98;
            this.lblRua.Text = "Rua:";
            // 
            // txtcomplementojuridico
            // 
            this.txtcomplementojuridico.Location = new System.Drawing.Point(343, 369);
            this.txtcomplementojuridico.MaxLength = 20;
            this.txtcomplementojuridico.Name = "txtcomplementojuridico";
            this.txtcomplementojuridico.Size = new System.Drawing.Size(100, 20);
            this.txtcomplementojuridico.TabIndex = 12;
            this.txtcomplementojuridico.TextChanged += new System.EventHandler(this.txtcomplemento_TextChanged);
            // 
            // lblComplemento
            // 
            this.lblComplemento.AutoSize = true;
            this.lblComplemento.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComplemento.Location = new System.Drawing.Point(230, 370);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.Size = new System.Drawing.Size(101, 18);
            this.lblComplemento.TabIndex = 96;
            this.lblComplemento.Text = "Complemento:";
            // 
            // txtnumerojuridico
            // 
            this.txtnumerojuridico.Location = new System.Drawing.Point(342, 220);
            this.txtnumerojuridico.MaxLength = 5;
            this.txtnumerojuridico.Name = "txtnumerojuridico";
            this.txtnumerojuridico.Size = new System.Drawing.Size(100, 20);
            this.txtnumerojuridico.TabIndex = 8;
            this.txtnumerojuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtncasa_KeyPress);
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(291, 257);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(36, 18);
            this.lblCEP.TabIndex = 94;
            this.lblCEP.Text = "CEP:";
            // 
            // txtreferenciajuridico
            // 
            this.txtreferenciajuridico.Location = new System.Drawing.Point(343, 337);
            this.txtreferenciajuridico.MaxLength = 20;
            this.txtreferenciajuridico.Name = "txtreferenciajuridico";
            this.txtreferenciajuridico.Size = new System.Drawing.Size(100, 20);
            this.txtreferenciajuridico.TabIndex = 11;
            this.txtreferenciajuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtreferencia_KeyPress);
            // 
            // lblReferencia
            // 
            this.lblReferencia.AutoSize = true;
            this.lblReferencia.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferencia.Location = new System.Drawing.Point(248, 337);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(82, 18);
            this.lblReferencia.TabIndex = 92;
            this.lblReferencia.Text = "Referência:";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(231, 223);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(112, 18);
            this.lblNumero.TabIndex = 90;
            this.lblNumero.Text = "Número da casa:";
            // 
            // dtpNascimentojuridico
            // 
            this.dtpNascimentojuridico.CustomFormat = "dd-mm-yyyy";
            this.dtpNascimentojuridico.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNascimentojuridico.Location = new System.Drawing.Point(116, 230);
            this.dtpNascimentojuridico.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpNascimentojuridico.MinDate = new System.DateTime(1900, 12, 31, 0, 0, 0, 0);
            this.dtpNascimentojuridico.Name = "dtpNascimentojuridico";
            this.dtpNascimentojuridico.Size = new System.Drawing.Size(109, 20);
            this.dtpNascimentojuridico.TabIndex = 4;
            this.dtpNascimentojuridico.ValueChanged += new System.EventHandler(this.dtpNascimento_ValueChanged);
            // 
            // lblBataNasc
            // 
            this.lblBataNasc.AutoSize = true;
            this.lblBataNasc.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBataNasc.Location = new System.Drawing.Point(29, 218);
            this.lblBataNasc.Name = "lblBataNasc";
            this.lblBataNasc.Size = new System.Drawing.Size(72, 36);
            this.lblBataNasc.TabIndex = 124;
            this.lblBataNasc.Text = "Data de \r\nFundação:";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Candara", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(59, 149);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(38, 15);
            this.lblCPF.TabIndex = 122;
            this.lblCPF.Text = "CNPJ:";
            // 
            // txtatividadejuridico
            // 
            this.txtatividadejuridico.Location = new System.Drawing.Point(116, 187);
            this.txtatividadejuridico.MaxLength = 18;
            this.txtatividadejuridico.Name = "txtatividadejuridico";
            this.txtatividadejuridico.Size = new System.Drawing.Size(109, 20);
            this.txtatividadejuridico.TabIndex = 3;
            this.txtatividadejuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprofissao_KeyPress);
            // 
            // lblProfissao
            // 
            this.lblProfissao.AutoSize = true;
            this.lblProfissao.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfissao.Location = new System.Drawing.Point(32, 184);
            this.lblProfissao.Name = "lblProfissao";
            this.lblProfissao.Size = new System.Drawing.Size(72, 18);
            this.lblProfissao.TabIndex = 120;
            this.lblProfissao.Text = "Atividade:";
            // 
            // txtrazaojuridico
            // 
            this.txtrazaojuridico.Location = new System.Drawing.Point(116, 107);
            this.txtrazaojuridico.MaxLength = 16;
            this.txtrazaojuridico.Name = "txtrazaojuridico";
            this.txtrazaojuridico.Size = new System.Drawing.Size(109, 20);
            this.txtrazaojuridico.TabIndex = 1;
            this.txtrazaojuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrazao_KeyPress);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(5, 109);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(91, 18);
            this.lblNome.TabIndex = 118;
            this.lblNome.Text = "Razão Social:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(93, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 126;
            this.label1.Text = "Cliente:";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::WindowsFormsApp3.Properties.Resources.IMG_20180826_WA0004;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(596, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 43);
            this.pictureBox4.TabIndex = 132;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // txtobsjuridico
            // 
            this.txtobsjuridico.Location = new System.Drawing.Point(12, 410);
            this.txtobsjuridico.MaxLength = 50;
            this.txtobsjuridico.Multiline = true;
            this.txtobsjuridico.Name = "txtobsjuridico";
            this.txtobsjuridico.Size = new System.Drawing.Size(545, 104);
            this.txtobsjuridico.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(9, 384);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 13);
            this.label3.TabIndex = 133;
            this.label3.Text = "Informações adicionais:";
            // 
            // MtxCEPjuridico
            // 
            this.MtxCEPjuridico.Location = new System.Drawing.Point(343, 256);
            this.MtxCEPjuridico.Mask = "00000-000";
            this.MtxCEPjuridico.Name = "MtxCEPjuridico";
            this.MtxCEPjuridico.Size = new System.Drawing.Size(100, 20);
            this.MtxCEPjuridico.TabIndex = 9;
            // 
            // MtbCnpjjuridico
            // 
            this.MtbCnpjjuridico.Location = new System.Drawing.Point(116, 149);
            this.MtbCnpjjuridico.Mask = "00.000.000/0000-00";
            this.MtbCnpjjuridico.Name = "MtbCnpjjuridico";
            this.MtbCnpjjuridico.Size = new System.Drawing.Size(109, 20);
            this.MtbCnpjjuridico.TabIndex = 2;
            this.MtbCnpjjuridico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox2_KeyPress);
            // 
            // mtbtelefonejuridico
            // 
            this.mtbtelefonejuridico.Location = new System.Drawing.Point(547, 105);
            this.mtbtelefonejuridico.Mask = "(00) 0000-0000";
            this.mtbtelefonejuridico.Name = "mtbtelefonejuridico";
            this.mtbtelefonejuridico.Size = new System.Drawing.Size(100, 20);
            this.mtbtelefonejuridico.TabIndex = 13;
            // 
            // mtbtelcomercialjuridico
            // 
            this.mtbtelcomercialjuridico.Location = new System.Drawing.Point(547, 187);
            this.mtbtelcomercialjuridico.Mask = "(00) 0000-0000";
            this.mtbtelcomercialjuridico.Name = "mtbtelcomercialjuridico";
            this.mtbtelcomercialjuridico.Size = new System.Drawing.Size(100, 20);
            this.mtbtelcomercialjuridico.TabIndex = 15;
            // 
            // mtbcelularjuridico
            // 
            this.mtbcelularjuridico.Location = new System.Drawing.Point(547, 140);
            this.mtbcelularjuridico.Mask = "(00) 00000-0000";
            this.mtbcelularjuridico.Name = "mtbcelularjuridico";
            this.mtbcelularjuridico.Size = new System.Drawing.Size(100, 20);
            this.mtbcelularjuridico.TabIndex = 14;
            // 
            // btnSalvarJuridico
            // 
            this.btnSalvarJuridico.BackgroundImage = global::WindowsFormsApp3.Properties.Resources.IMG_20180826_WA0003;
            this.btnSalvarJuridico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSalvarJuridico.Location = new System.Drawing.Point(570, 447);
            this.btnSalvarJuridico.Name = "btnSalvarJuridico";
            this.btnSalvarJuridico.Size = new System.Drawing.Size(81, 67);
            this.btnSalvarJuridico.TabIndex = 129;
            this.btnSalvarJuridico.TabStop = false;
            this.btnSalvarJuridico.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // CadastrarClienteJuridico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 532);
            this.Controls.Add(this.mtbcelularjuridico);
            this.Controls.Add(this.mtbtelcomercialjuridico);
            this.Controls.Add(this.mtbtelefonejuridico);
            this.Controls.Add(this.MtbCnpjjuridico);
            this.Controls.Add(this.MtxCEPjuridico);
            this.Controls.Add(this.txtobsjuridico);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.btnSalvarJuridico);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpNascimentojuridico);
            this.Controls.Add(this.lblBataNasc);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.txtatividadejuridico);
            this.Controls.Add(this.lblProfissao);
            this.Controls.Add(this.txtrazaojuridico);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.txtEmailjuridico);
            this.Controls.Add(this.lblRamal);
            this.Controls.Add(this.txtramaljuridico);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblCelular);
            this.Controls.Add(this.lblTelComercial);
            this.Controls.Add(this.lblTelefone);
            this.Controls.Add(this.lblContato);
            this.Controls.Add(this.cboEstadojuridico);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.txtcidadejuridico);
            this.Controls.Add(this.lblCidade);
            this.Controls.Add(this.txtbairrojuridico);
            this.Controls.Add(this.lblBairro);
            this.Controls.Add(this.txtRuajuridico);
            this.Controls.Add(this.lblRua);
            this.Controls.Add(this.txtcomplementojuridico);
            this.Controls.Add(this.lblComplemento);
            this.Controls.Add(this.txtnumerojuridico);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.txtreferenciajuridico);
            this.Controls.Add(this.lblReferencia);
            this.Controls.Add(this.lblNumero);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastrarClienteJuridico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastrarClienteJuridico";
            this.Load += new System.EventHandler(this.CadastrarClienteJuridico_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalvarJuridico)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEmailjuridico;
        private System.Windows.Forms.Label lblRamal;
        private System.Windows.Forms.TextBox txtramaljuridico;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.Label lblTelComercial;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Label lblContato;
        private System.Windows.Forms.ComboBox cboEstadojuridico;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.TextBox txtcidadejuridico;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.TextBox txtbairrojuridico;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.TextBox txtRuajuridico;
        private System.Windows.Forms.Label lblRua;
        private System.Windows.Forms.TextBox txtcomplementojuridico;
        private System.Windows.Forms.Label lblComplemento;
        private System.Windows.Forms.TextBox txtnumerojuridico;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.TextBox txtreferenciajuridico;
        private System.Windows.Forms.Label lblReferencia;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.DateTimePicker dtpNascimentojuridico;
        private System.Windows.Forms.Label lblBataNasc;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.TextBox txtatividadejuridico;
        private System.Windows.Forms.Label lblProfissao;
        private System.Windows.Forms.TextBox txtrazaojuridico;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox txtobsjuridico;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox MtxCEPjuridico;
        private System.Windows.Forms.MaskedTextBox MtbCnpjjuridico;
        private System.Windows.Forms.MaskedTextBox mtbtelefonejuridico;
        private System.Windows.Forms.MaskedTextBox mtbtelcomercialjuridico;
        private System.Windows.Forms.MaskedTextBox mtbcelularjuridico;
        private System.Windows.Forms.PictureBox btnSalvarJuridico;
    }
}