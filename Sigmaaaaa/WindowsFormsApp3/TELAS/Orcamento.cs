﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3;
using WindowsFormsApp3.Programação;
using WindowsFormsApp3.Programação.CadastrarVeiculo;
using WindowsFormsApp3.Programação.Orçamento;
using WindowsFormsApp3.Programação.Produto;
using WindowsFormsApp3.Programação.Serviço;
using WindowsFormsApp3.TELAS;

namespace telas_do_SIGMA
{
    public partial class Orçamento : Form
    {
        Validação v = new Validação();
        BindingList<ProdutoDTO> produtos = new BindingList<ProdutoDTO>();
        BindingList<ServiçoDTO> servicos = new BindingList<ServiçoDTO>();

        public Orçamento()
        {
            InitializeComponent();
            CarregarCombos();

            dataGridView3.AutoGenerateColumns = false;
            dataGridView3.DataSource = produtos;

            dataGridView4.AutoGenerateColumns = false;
            dataGridView4.DataSource = servicos;
        }

        void CarregarCombos()
        {
            ProdutoBusiness bus = new ProdutoBusiness();
            List<ProdutoDTO> lista = bus.Listar();
            cmbteste.DisplayMember = nameof(ProdutoDTO.nomeproduto);
            cmbteste.ValueMember = nameof(ProdutoDTO.id);
            cmbteste.DataSource = lista;

            ServiçoBusiness eae = new ServiçoBusiness();
            List<ServiçoDTO> lista1 = eae.Listar();
            cmbservico.DisplayMember = nameof(ServiçoDTO.nomeservico);
            cmbservico.ValueMember = nameof(ServiçoDTO.id);
            cmbservico.DataSource = lista1;


            VeiculoBusiness busies = new VeiculoBusiness();
            List<VeiculoDTO> list = busies.Listar();
            cmbplaca.DisplayMember = nameof(VeiculoDTO.placa);
            cmbplaca.ValueMember = nameof(VeiculoDTO.idveiculo);
            cmbplaca.DataSource = list;


        }


        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TeladeMenu tela = new TeladeMenu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            OrcamentoItemDTO dto = new OrcamentoItemDTO();
            dto.dataorcamento = DateTime.Now;
            dto.falha = txtfalha.Text;
            VeiculoDTO vei = cmbplaca.SelectedItem as VeiculoDTO;
            dto.idveiculo = vei.idveiculo;


            OrcamentoItemBusiness business = new OrcamentoItemBusiness();
            
            business.Salvar(dto, produtos.ToList(), servicos.ToList());

            MessageBox.Show("Orçamento salvo com sucesso");


        }

        private void button8_Click(object sender, EventArgs e)
        {
           
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
        }

        private void Orçamento_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CadastrarServico tela = new CadastrarServico();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
  
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            ProdutoDTO produto = cmbteste.SelectedItem as ProdutoDTO;
            for (int i = 0; i < Convert.ToInt32(txtquantidadeproduto.Text); i++)
            {
                produtos.Add(produto);
            }
        }

        private void dgvproduto1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void cmbteste_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtquantidadeproduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtquantidadeproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ServiçoDTO servico = cmbservico.SelectedItem as ServiçoDTO;
            for (int i = 0; i < Convert.ToInt32(txtquantidadeservico.Text); i++)
            {
                servicos.Add(servico);
            }
        }

        private void txtquantidadeservico_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
