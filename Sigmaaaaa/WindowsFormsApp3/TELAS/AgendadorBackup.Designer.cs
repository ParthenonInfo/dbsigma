﻿namespace WindowsFormsApp3
{
    partial class AgendadorBackup
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgendadorBackup));
            this.label1 = new System.Windows.Forms.Label();
            this.BackupTextBox = new System.Windows.Forms.TextBox();
            this.BrowseBackupBtn = new System.Windows.Forms.PictureBox();
            this.TakeBackupBtn = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BrowseBackupBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TakeBackupBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pasta para Backup";
            // 
            // BackupTextBox
            // 
            this.BackupTextBox.Location = new System.Drawing.Point(15, 49);
            this.BackupTextBox.Name = "BackupTextBox";
            this.BackupTextBox.Size = new System.Drawing.Size(205, 20);
            this.BackupTextBox.TabIndex = 1;
            // 
            // BrowseBackupBtn
            // 
            this.BrowseBackupBtn.Image = ((System.Drawing.Image)(resources.GetObject("BrowseBackupBtn.Image")));
            this.BrowseBackupBtn.Location = new System.Drawing.Point(226, 51);
            this.BrowseBackupBtn.Name = "BrowseBackupBtn";
            this.BrowseBackupBtn.Size = new System.Drawing.Size(33, 31);
            this.BrowseBackupBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BrowseBackupBtn.TabIndex = 2;
            this.BrowseBackupBtn.TabStop = false;
            this.BrowseBackupBtn.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // TakeBackupBtn
            // 
            this.TakeBackupBtn.Image = ((System.Drawing.Image)(resources.GetObject("TakeBackupBtn.Image")));
            this.TakeBackupBtn.Location = new System.Drawing.Point(417, 68);
            this.TakeBackupBtn.Name = "TakeBackupBtn";
            this.TakeBackupBtn.Size = new System.Drawing.Size(81, 67);
            this.TakeBackupBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.TakeBackupBtn.TabIndex = 5;
            this.TakeBackupBtn.TabStop = false;
            this.TakeBackupBtn.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(443, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // AgendadorBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 140);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.TakeBackupBtn);
            this.Controls.Add(this.BrowseBackupBtn);
            this.Controls.Add(this.BackupTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AgendadorBackup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Backup";
            ((System.ComponentModel.ISupportInitialize)(this.BrowseBackupBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TakeBackupBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BackupTextBox;
        private System.Windows.Forms.PictureBox BrowseBackupBtn;
        private System.Windows.Forms.PictureBox TakeBackupBtn;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}

