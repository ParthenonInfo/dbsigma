﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação;
using WindowsFormsApp3.Programação.Cliente;

namespace WindowsFormsApp3.TELAS
{
    public partial class ControledeAcesso : Form
    {
        public ControledeAcesso()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //validações
            FuncionarioBusiness business = new FuncionarioBusiness();
            FuncionarioDTO funcionario = business.Logar(txtLogin.Text, txtSenha.Text);
            if (funcionario != null)
            {
                UserSession.UsuarioLogado = funcionario;
                TeladeMenu menu = new TeladeMenu();
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.");
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
