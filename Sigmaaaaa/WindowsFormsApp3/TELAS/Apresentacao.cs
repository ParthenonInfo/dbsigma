﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.TELAS;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(6000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                   ControledeAcesso frm = new ControledeAcesso();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (pbcarrega.Value < 100)
            {
                pbcarrega.Value = pbcarrega.Value + 2;
            }
            else
            {
                timer1.Enabled = false;
                this.Visible = false;
                //Colocar pra onde a tela vai depois de carregar o splash!
                //frm_login login = new frm_login();
                //login.ShowDialog();
            }

        }
    }
}
