﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using telas_do_SIGMA;
using WindowsFormsApp3.TELAS;

namespace WindowsFormsApp3
{
    public partial class TeladeMenu : Form
    {
        public TeladeMenu()
        {
            InitializeComponent();
        }

        private void btnAgendarBackup_Click(object sender, EventArgs e)
        {
            AgendadorBackup tela = new AgendadorBackup();
            tela.Show();
            this.Hide();
        }

        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            CadastrarClienteGerais tela = new CadastrarClienteGerais();
            tela.Show();
            this.Hide();
        }

        private void btnCadastrarFuncionario_Click(object sender, EventArgs e)
        {
          CadastrarFuncionario tela = new CadastrarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void btnCadastrarVeiculo_Click(object sender, EventArgs e)
        {
            CadastrarVeiculo tela = new CadastrarVeiculo();
            tela.Show();
            this.Hide();

        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
            Orçamento tela = new Orçamento();
            tela.Show();
            this.Hide();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Você quer mesmo sair?", "Sigma",
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Sobre tela = new Sobre();
            tela.Show();
            this.Hide();
        }

        private void btnPesqAuxiliar_Click(object sender, EventArgs e)
        {
            LocalizarVeiculo tela = new LocalizarVeiculo();
            tela.Show();
            this.Hide();
        }

        private void Btncontroledeacesso_Click(object sender, EventArgs e)
        {
            ControledeAcesso tela = new ControledeAcesso();
            tela.Show();
            this.Hide();
        }

        private void TeladeMenu_Load(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            CadastrarClienteJuridico tela = new CadastrarClienteJuridico();
            tela.Show();
            this.Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ControledeAcesso tela = new ControledeAcesso();
            tela.Show();
            this.Hide();
        }
    }
}
