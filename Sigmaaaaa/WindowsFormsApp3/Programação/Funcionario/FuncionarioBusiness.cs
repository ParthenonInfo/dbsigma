﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Cliente
{
    
    
    public class FuncionarioBusiness
    {
        FuncionarioDatabase db = new FuncionarioDatabase();

        public int Salvar(FuncionarioDTO funcionario)
        {
            if (funcionario.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (funcionario.rg == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório.");
            }

            if (funcionario.cpf == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }
            if (funcionario.celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório.");
            }
            if (funcionario.nascimento > DateTime.Now)
            {
                throw new ArgumentException("Data não valida");
            }
            if (funcionario.nascimento.Year > 1999)
            {
                throw new ArgumentException("tem que ser maior de 18");
            }
            return db.Salvar(funcionario);
        }

        public void Alterar(FuncionarioDTO funcionario)
        {
            if (funcionario.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (funcionario.rg == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório.");
            }
            if (funcionario.cpf == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }
            if (funcionario.celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório.");
            }
            db.Alterar(funcionario);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }

        public FuncionarioDTO Logar(string login, string senha)
        {
            if (login == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório");
            }
            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(login, senha);
        }
    
    }
}

