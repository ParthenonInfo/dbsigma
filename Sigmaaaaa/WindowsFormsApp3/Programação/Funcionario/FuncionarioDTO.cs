﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Cliente
{
    public class FuncionarioDTO
    {
        public int idfuncionario { get; set; }

        public string nome { get; set; }

        public string cpf { get; set; }

        public string rg { get; set; }

        public int iddepartamento { get; set; }

        public DateTime nascimento { get; set; }

        public string telefone { get; set; }

        public string celular { get; set; }

        public string email { get; set; }

        public int idendereco { get; set; }
       
        public string  usuario { get; set; }
        public string senha { get; set; }

    }
}
