﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Cliente
{
    public class FuncionarioDatabase
    {
       
        public int Salvar(FuncionarioDTO funcionario)
        {
            string script =
                @"INSERT INTO tb_funcionario
                (
                 idFuncionario,
                 id_Endereco,
                 nm_funcionario,
                 id_departamento,
                 ds_cpf,
                 ds_rg,
                 dt_nascimento,
                 ds_telefone,
                 ds_celular,
                 ds_email,
                 nm_usuario,
                 ds_senha
                )
                VALUES
                (
                  @idFuncionario,
                  @id_Endereco,
                  @nm_funcionario,
                  @id_departamento,
                  @ds_cpf,
                  @ds_rg,
                  @dt_nascimento,
                  @ds_telefone,
                  @ds_celular,
                  @ds_email,
                  @nm_usuario,
                  @ds_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFuncionario", funcionario.idfuncionario));
            parms.Add(new MySqlParameter("id_Endereco", funcionario.idendereco));
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.nome));
            parms.Add(new MySqlParameter("id_departamento", funcionario.iddepartamento));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.cpf));
            parms.Add(new MySqlParameter("ds_rg", funcionario.rg));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.nascimento));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.celular));
            parms.Add(new MySqlParameter("ds_email", funcionario.email));
            parms.Add(new MySqlParameter("nm_usuario", funcionario.usuario));
            parms.Add(new MySqlParameter("ds_senha", funcionario.senha));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(FuncionarioDTO funcionario)
        {
            string script =
            @"UPDATE tb_funcionario
                 SET idFuncionario = @idFuncionario,
                     id_Endereco=@id_Endereco,
	                 nm_funcionario = @nm_funcionario,
	                 id_departamento = @id_departamento,
	                 ds_cpf = @ds_cpf,
	                 dt_nascimento = @dt_nascimento,
                     ds_telefone= @ ds_telefone,
                     ds_celular= @ds_celular,
                     ds_email= @ds_email,
                     nm_usuario=@nm_usuario,
                     ds_senha=@ds_senha
	                 WHERE idFuncionario = @idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFuncionario", funcionario.idfuncionario));
            parms.Add(new MySqlParameter("id_Endereco", funcionario.idendereco));
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.nome));
            parms.Add(new MySqlParameter("id_departamento", funcionario.iddepartamento));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.cpf));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.telefone));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.nascimento));
            parms.Add(new MySqlParameter("ds_celular", funcionario.celular));
            parms.Add(new MySqlParameter("ds_email", funcionario.email));
            parms.Add(new MySqlParameter("nm_usuario", funcionario.usuario));
            parms.Add(new MySqlParameter("ds_senha", funcionario.senha));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_funcionario WHERE idFuncionario= @idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFuncionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FuncionarioDTO> Listar()
        {
            string script =
                @"SELECT * FROM tb_funcionario ORDER BY idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> funcionarios = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO novoFuncionario = new FuncionarioDTO();
                novoFuncionario.idfuncionario = reader.GetInt32("idFuncionario");
                novoFuncionario.idendereco = reader.GetInt32("id_Endereco");
                novoFuncionario.iddepartamento = reader.GetInt32("id_departamento");
                novoFuncionario.nome = reader.GetString("nm_funcionario");
                novoFuncionario.cpf = reader.GetString("ds_cpf");
                novoFuncionario.rg = reader.GetString("ds_rg");
                novoFuncionario.nascimento = reader.GetDateTime("dt_nascimento");
                novoFuncionario.telefone = reader.GetString("ds_telefone");
                novoFuncionario.celular = reader.GetString("ds_celular");
                novoFuncionario.email = reader.GetString("ds_email");
                novoFuncionario.usuario = reader.GetString("nm_usuario");
                novoFuncionario.senha = reader.GetString("ds_senha");

                funcionarios.Add(novoFuncionario);
            }
            reader.Close();

            return funcionarios;
        }

            public FuncionarioDTO Logar (string login, string senha)
            {
                string script = @"SELECT * FROM tb_funcionario
                                 WHERE nm_usuario = @nm_usuario
                                 AND ds_senha = @ds_senha";
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_usuario", login));
                parms.Add(new MySqlParameter("ds_senha", senha));

                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
                FuncionarioDTO funcionario = null;
                if (reader.Read())
                {
                    funcionario = new FuncionarioDTO();
                    funcionario.idfuncionario= reader.GetInt32("idFuncionario");
                    funcionario.idendereco = reader.GetInt32("id_Endereco");
                    funcionario.iddepartamento = reader.GetInt32("id_departamento");
                    funcionario.nascimento = reader.GetDateTime("dt_nascimento");
                    funcionario.nome = reader.GetString("nm_funcionario");
                    funcionario.rg = reader.GetString("ds_rg");
                    funcionario.senha = reader.GetString("ds_senha");
                    funcionario.telefone = reader.GetString("ds_telefone");
                    funcionario.usuario = reader.GetString("nm_usuario");
                    funcionario.celular = reader.GetString("ds_celular");
                    funcionario.cpf = reader.GetString("ds_cpf");
                    funcionario.email = reader.GetString("ds_email");
                }
                reader.Close();
                return funcionario;

            }

    }
}

