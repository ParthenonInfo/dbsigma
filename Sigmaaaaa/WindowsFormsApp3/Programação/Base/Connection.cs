﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Base
{
   public class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=DBsigma;uid=root;password=1234 ;sslmode=none";
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
