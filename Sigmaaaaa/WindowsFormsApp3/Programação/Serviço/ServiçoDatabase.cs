﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Serviço
{
 public  class ServiçoDatabase
    {
        public int Salvar(ServiçoDTO serviço)
        {
            string script =
                @"INSERT INTO tb_servico
                (
                    id_servico,
                    nm_servico,
                    vl_preco,
                    ds_descricao
                )
                VALUES
                (
                    @id_servico,
                    @nm_servico,
                    @vl_preco,
                    @ds_descricao
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_servico", serviço.id));
            parms.Add(new MySqlParameter("nm_servico", serviço.nomeservico));
            parms.Add(new MySqlParameter("vl_preco", serviço.preco));
            parms.Add(new MySqlParameter("ds_descricao", serviço.descricao));
          
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ServiçoDTO serviço)
        {
            string script =
            @"UPDATE tb_servico
                 SET id_servico  = @id_servico,
                  nm_servico= @nm_servico,
                  vl_preco = @vl_preco,
                  ds_descricao= @ds_descricao,
                  WHERE id_servico= @id_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_servico",serviço.id));
            parms.Add(new MySqlParameter("nm_servico", serviço.nomeservico));
            parms.Add(new MySqlParameter("vl_preco", serviço.preco));
            parms.Add(new MySqlParameter("ds_descricao", serviço.descricao));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_servico WHERE id_servico= @id_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_servico", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ServiçoDTO> Consultar(string serviço)
        {
            string script =
                @"SELECT * FROM tb_servico
                  WHERE nm_servico like @nm_servico";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_servico", "%" + serviço + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ServiçoDTO> servicos = new List<ServiçoDTO>();
            while(reader.Read())
            {
            
                    ServiçoDTO novoserviço = new ServiçoDTO();
                    novoserviço.id = reader.GetInt32("id_servico");
                    novoserviço.nomeservico = reader.GetString("nm_servico");
                    novoserviço.preco = reader.GetDecimal("vl_preco");
                    novoserviço.descricao = reader.GetString("ds_descricao");
                    servicos.Add(novoserviço);
               
            }
            return servicos;

        }

        public List<ServiçoDTO> Listar()
        {
            string script =
               @"SELECT * FROM tb_servico";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ServiçoDTO> servicos = new List<ServiçoDTO>();
            while (reader.Read())
            {

                ServiçoDTO novoserviço = new ServiçoDTO();
                novoserviço.id = reader.GetInt32("id_servico");
                novoserviço.nomeservico = reader.GetString("nm_servico");
                novoserviço.preco = reader.GetDecimal("vl_preco");
                novoserviço.descricao = reader.GetString("ds_descricao");
                servicos.Add(novoserviço);

            }
            return servicos;
        }
    }
}
