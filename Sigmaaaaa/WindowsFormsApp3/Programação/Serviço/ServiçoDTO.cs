﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Serviço
{
  public  class ServiçoDTO
    {
        public int id { get; set; }
        public string nomeservico { get; set; }
        public decimal preco { get; set; }
        public string descricao { get; set; }
    }
}
