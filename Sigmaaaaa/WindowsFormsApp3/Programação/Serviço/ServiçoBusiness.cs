﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Serviço
{
  public  class ServiçoBusiness
    {
        ServiçoDatabase db = new ServiçoDatabase();
        public int Salvar(ServiçoDTO serviço)
        {
            if (serviço.nomeservico== string.Empty)
            {
                throw new ArgumentException("Nome do serviço é obrigatório.");
            }
            if (serviço.preco< 1)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }


            return db.Salvar(serviço);
        }

        public void Alterar(ServiçoDTO serviço)
        {
            if (serviço.nomeservico == string.Empty)
            {
                throw new ArgumentException("Nome do serviço é obrigatório.");
            }
            if (serviço.preco < 1)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }
            db.Alterar(serviço);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<ServiçoDTO> Consultar(string serviço)
        {
            return db.Consultar(serviço);
        }

        public List<ServiçoDTO> Listar()
        {
            return db.Listar();
        }
    }
}
