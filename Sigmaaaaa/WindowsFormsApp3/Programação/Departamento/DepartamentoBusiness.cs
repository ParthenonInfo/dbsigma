﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Departamento
{
    class DepartamentoBusiness
    {
        DepartamentoDatabase db = new DepartamentoDatabase();
        public List<DepartamentoDTO> Listar()
        {
            return db.Listar();
        }
    }
}
