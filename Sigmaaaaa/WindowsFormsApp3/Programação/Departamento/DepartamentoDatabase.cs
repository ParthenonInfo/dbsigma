﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Departamento
{
    class DepartamentoDatabase
    {
        public List<DepartamentoDTO>Listar()
        {
            string script =
                @"SELECT * FROM tb_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DepartamentoDTO> lista = new List<DepartamentoDTO>();

            while(reader.Read())
            {
                DepartamentoDTO det = new DepartamentoDTO();
                det.Id = reader.GetInt32("id_departamento");
                det.Nome = reader.GetString("nm_departamento");
                lista.Add(det);
            }
            reader.Close();
            return lista;

        }

    }
}
