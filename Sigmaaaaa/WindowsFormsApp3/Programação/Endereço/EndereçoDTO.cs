﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Endereço
{
    public class EndereçoDTO
    {
        public int idendereco { get; set; }

        public string rua { get; set; }

        public string bairro { get; set; }

        public string cidade { get; set; }

        public string referencia { get; set; }

        public string complemento { get; set; }

        public string estado { get; set; }

        public string numerocasa { get; set; }

        public string cep { get; set; }

    }
}
