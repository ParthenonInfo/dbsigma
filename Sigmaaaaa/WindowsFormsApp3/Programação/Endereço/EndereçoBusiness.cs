﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Endereço
{
    public class EndereçoBusiness
    {
        EndereçoDatabase db = new EndereçoDatabase();

        public int Salvar(EndereçoDTO endereço)
        {
            if (endereço.rua == string.Empty)
            {
                throw new ArgumentException("Rua é obrigatória.");
            }
            if (endereço.referencia== string.Empty)
            {
                throw new ArgumentException("Referência é obrigatória.");
            }
            if (endereço.numerocasa== string.Empty)
            {
                throw new ArgumentException("Numero de casa é obrigatório.");
            }
            if (endereço.estado== string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório.");
            }
            if (endereço.cidade== string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório.");
            }

            if (endereço.cep== string.Empty)
            {
                throw new ArgumentException("Cep é obrigatório.");
            }

            if (endereço.bairro== string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório.");
            }
            return db.Salvar(endereço);
        }

        public void Alterar(EndereçoDTO endereço)
        {
            if (endereço.rua == string.Empty)
            {
                throw new ArgumentException("Rua é obrigatória.");
            }
            if (endereço.referencia == string.Empty)
            {
                throw new ArgumentException("Referência é obrigatória.");
            }
            if (endereço.numerocasa == string.Empty)
            {
                throw new ArgumentException("Numero de casa é obrigatório.");
            }
            if (endereço.estado == string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório.");
            }
            if (endereço.cidade == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório.");
            }

            if (endereço.cep == string.Empty)
            {
                throw new ArgumentException("Cep é obrigatório.");
            }

            if (endereço.bairro == string.Empty)
            {
                throw new ArgumentException("Bairro é obrigatório.");
            }
            db.Alterar(endereço);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }


    }
}
