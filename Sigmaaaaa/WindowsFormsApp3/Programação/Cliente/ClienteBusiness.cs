﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Cliente
{
    public class ClienteBusiness
    {
        ClienteDatabase db = new ClienteDatabase();
        
        public int Salvar(ClienteDTO cliente)
        {
            if (cliente.Nome== string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (cliente.telefone== string.Empty)
            {
                throw new ArgumentException("telefone é obrigatório.");
            }
            if (cliente.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório.");
            }
            if (cliente.cpf == string.Empty)
            {
                throw new ArgumentException("cpf é obrigatório.");
            }
            if (cliente.celular == string.Empty)
            {
                throw new ArgumentException("celular é obrigatório.");
            }

            if (cliente.nascimento > DateTime.Now)
            {
                throw new ArgumentException("Data não valida");
            }

            if (cliente.nascimento.Year > 1999)
            {
                throw new ArgumentException("tem que ser maior de 18");
            }

            return db.Salvar(cliente);
            
        }

        public void Alterar(ClienteDTO cliente)
        {
            if (cliente.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (cliente.telefone == string.Empty)
            {
                throw new ArgumentException("telefone é obrigatório.");
            }
            if (cliente.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório.");
            }
            if (cliente.cpf == string.Empty)
            {
                throw new ArgumentException("cpf é obrigatório.");
            }
            if (cliente.celular == string.Empty)
            {
                throw new ArgumentException("celular é obrigatório.");
            }
            
            db.Alterar(cliente);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

    }
}
