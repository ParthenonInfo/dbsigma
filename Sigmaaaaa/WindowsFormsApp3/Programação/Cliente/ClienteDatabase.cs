﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Cliente
{
    public class ClienteDatabase
    {

        public int Salvar(ClienteDTO cliente)
        {
            string script =
                @"INSERT INTO tb_cliente
                (
                   id_cliente,
                   id_Endereco ,
                   nm_cliente,
                   ds_cpf ,
                   ds_profissao,
                    dt_nascimento,
                    ds_telefone,
                    ds_celular,
                    ds_telcomercial,
                    ds_email,
                    ds_ramal,
                    ds_cnpj,
                    ds_razaosocial,
                    ds_atividade,   
                    ds_clienteobs
                )
                VALUES
                (
                    @id_cliente,
                   @id_Endereco,
                  
                   @nm_cliente,
                   @ds_cpf,
                   @ds_profissao,
                    @dt_nascimento,
                    @ds_telefone,
                    @ds_celular,
                    @ds_telcomercial,
                    @ds_email,
                    @ds_ramal,
                    @ds_cnpj,
                    @ds_razaosocial,
                    @ds_atividade,   
                    @ds_clienteobs)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", cliente.Id_cliente));
            parms.Add(new MySqlParameter("id_Endereco", cliente.Id_Endereco));
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("ds_cpf", cliente.cpf));
            parms.Add(new MySqlParameter("ds_profissao", cliente.profissao));
            parms.Add(new MySqlParameter("dt_nascimento", cliente.nascimento));
            parms.Add(new MySqlParameter("ds_telefone", cliente.telefone));
            parms.Add(new MySqlParameter("ds_celular", cliente.celular));
            parms.Add(new MySqlParameter("ds_telcomercial", cliente.telcomercial));
            parms.Add(new MySqlParameter("ds_email", cliente.email));
            parms.Add(new MySqlParameter("ds_cnpj", cliente.cnpj));
            parms.Add(new MySqlParameter("ds_ramal", cliente.ramal));
            parms.Add(new MySqlParameter("ds_razaosocial", cliente.razaosocial));
            parms.Add(new MySqlParameter("ds_atividade", cliente.atividade));
            parms.Add(new MySqlParameter("ds_clienteobs", cliente.clienteObs));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ClienteDTO cliente)
        {
            string script =
            @"UPDATE tb_cliente
                SET id_cliente  = @id_cliente,
	                id_Endereco = @id_Endereco
	                nm_cliente = @nm_cliente,
	                ds_profissao = @ds_profissao,
	                dt_nascimento   = @dt_nascimento ,
	                ds_telefone = @ds_telefone,
                    ds_celular = @ds_celular,
                    ds_telcomercial = @ds_telcomercial,
                    ds_email = @ds_email ,
                    ds_ramal = @ds_ramal,
                    ds_cnpj = @ds_cnpj,
                    ds_razaosocial =@ds_razaosocial ,
                    ds_atividade = @ds_atividade,
                    ds_clienteobs=@ds_clienteobs
	                 WHERE idClientes = @idClientes";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", cliente.Id_cliente));
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("id_Endereco", cliente.Id_Endereco));
            parms.Add(new MySqlParameter("ds_profissao", cliente.profissao));
            parms.Add(new MySqlParameter("dt_nascimento", cliente.nascimento));
            parms.Add(new MySqlParameter("ds_telefone", cliente.telefone));
            parms.Add(new MySqlParameter("ds_celular", cliente.celular));
            parms.Add(new MySqlParameter("ds_telcomercial", cliente.telcomercial));
            parms.Add(new MySqlParameter("ds_email", cliente.email));
            parms.Add(new MySqlParameter("ds_ramal", cliente.ramal));
            parms.Add(new MySqlParameter("ds_cnpj", cliente.cnpj));
            parms.Add(new MySqlParameter("ds_razaosocial", cliente.razaosocial));
            parms.Add(new MySqlParameter("ds_atividade", cliente.atividade));
            parms.Add(new MySqlParameter("ds_clienteobs", cliente.clienteObs));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_cliente WHERE id_cliente= @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


    }
}

