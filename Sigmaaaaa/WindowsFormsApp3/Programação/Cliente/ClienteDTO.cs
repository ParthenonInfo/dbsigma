﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Cliente
{
  public  class ClienteDTO
    {
        public int Id_cliente { get; set; }

        public int Id_Endereco { get; set; }

        public string Nome { get; set; }

        public String UF { get; set; }

        public string cpf { get; set; }

        public string profissao   { get; set; }

        public DateTime nascimento { get; set; }

        public string telefone { get; set; }

        public string celular { get; set; }

        public string telcomercial { get; set; }

        public string email { get; set; }

        public string ramal { get; set; }

        public string cnpj { get; set; }

        public string razaosocial { get; set; }

        public string atividade { get; set; }

        public string clienteObs { get; set; }
    }
}
