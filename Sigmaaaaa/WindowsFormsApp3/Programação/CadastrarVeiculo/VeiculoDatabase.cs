﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.CadastrarVeiculo
{
    public class VeiculoDatabase
    {
        public int Salvar(VeiculoDTO veiculo)
        {
            string script =
                @"INSERT INTO tb_veiculo
                (
                   id_veiculo,
                   id_cliente,
                   ds_placa,
                   ds_combustivel,
                   ds_odometro,
                   ds_cor,
                   ds_ano,
                   nm_proprietario,
                   ds_modelo,
                   ds_info
                )
                VALUES
                (
                   @id_veiculo,
                   @id_cliente,
                   @ds_placa,
                   @ds_combustivel,
                   @ds_odometro,
                   @ds_cor,
                   @ds_ano,
                   @nm_proprietario,
                   @ds_modelo,
                   @ds_info
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_veiculo", veiculo.idveiculo));
            parms.Add(new MySqlParameter("id_cliente", veiculo.idcliente));
            parms.Add(new MySqlParameter("ds_placa", veiculo.placa));
            parms.Add(new MySqlParameter("ds_combustivel", veiculo.combustivel));
            parms.Add(new MySqlParameter("ds_odometro", veiculo.odometro));
            parms.Add(new MySqlParameter("ds_cor", veiculo.cor));
            parms.Add(new MySqlParameter("ds_ano", veiculo.ano));
            parms.Add(new MySqlParameter("nm_proprietario", veiculo.proprietario));
            parms.Add(new MySqlParameter("ds_modelo", veiculo.modelo));
            parms.Add(new MySqlParameter("ds_info", veiculo.Informação));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(VeiculoDTO veiculo)
        {
            string script =
            @"UPDATE tb_veiculo
                 SET id_veiculo = @id_veiculo,
                  id_cliente=@id_cliente,
                  ds_placa = @ds_placa,
                  ds_combustivel = @ds_combustivel,
                  ds_odometro = @ds_odometro,
                  ds_cor  = @ds_cor,
                  ds_ano = @ds_ano,
                  WHERE id_veiculo = @id_veiculo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_veiculo", veiculo.idveiculo));
            parms.Add(new MySqlParameter("id_cliente", veiculo.idcliente));
            parms.Add(new MySqlParameter("ds_placa", veiculo.placa));
            parms.Add(new MySqlParameter("ds_combustivel", veiculo.combustivel));
            parms.Add(new MySqlParameter("ds_odometro", veiculo.odometro));
            parms.Add(new MySqlParameter("ds_cor", veiculo.cor));
            parms.Add(new MySqlParameter("ds_ano", veiculo.ano));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_veiculo WHERE id_veiculo = @id_veiculo ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_veiculo", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<VeiculoDTO> Consultar(string modelo)
        {
            string script =
                @"SELECT * FROM tb_veiculo
                  WHERE ds_modelo like @ds_modelo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + modelo + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VeiculoDTO> veiculos = new List<VeiculoDTO>();
            while (reader.Read())
            {
                VeiculoDTO novoveiculo = new VeiculoDTO();
                novoveiculo.placa = reader.GetString("ds_placa");
                novoveiculo.idveiculo = reader.GetInt32("id_veiculo");
                novoveiculo.combustivel = reader.GetString("ds_combustivel");
                novoveiculo.odometro = reader.GetString("ds_odometro");
                novoveiculo.cor = reader.GetString("ds_cor");
                novoveiculo.ano = reader.GetString("ds_ano");
                novoveiculo.proprietario = reader.GetString("nm_proprietario");
                novoveiculo.modelo = reader.GetString("ds_modelo");
                novoveiculo.Informação = reader.GetString("ds_info");

                veiculos.Add(novoveiculo);
            }
            return veiculos;
        }
        public List<VeiculoDTO> Listar()
        {
            string script =
                 @"SELECT * FROM tb_veiculo";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VeiculoDTO> veiculos = new List<VeiculoDTO>();
            while (reader.Read())
            {
                VeiculoDTO novoveiculo = new VeiculoDTO();
                novoveiculo.placa = reader.GetString("ds_placa");
                novoveiculo.idveiculo = reader.GetInt32("id_veiculo");
                novoveiculo.combustivel = reader.GetString("ds_combustivel");
                novoveiculo.odometro = reader.GetString("ds_odometro");
                novoveiculo.cor = reader.GetString("ds_cor");
                novoveiculo.ano = reader.GetString("ds_ano");
                novoveiculo.proprietario = reader.GetString("nm_proprietario");
                novoveiculo.modelo = reader.GetString("ds_modelo");
                novoveiculo.Informação = reader.GetString("ds_info");

                veiculos.Add(novoveiculo);
            }
            return veiculos;



        }

    }




  
}

