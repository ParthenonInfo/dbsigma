﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.CadastrarVeiculo
{
    public class VeiculoBusiness
    {
        VeiculoDatabase db = new VeiculoDatabase();

        public int Salvar(VeiculoDTO veiculo)
        {
            if (veiculo.placa == string.Empty)
            {
                throw new ArgumentException("Placa é obrigatória.");
            }
            if (veiculo.ano == string.Empty)
            {
                throw new ArgumentException("Ano é obrigatório.");
            }
            if (veiculo.cor == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatória.");
            }

            return db.Salvar(veiculo);
        }

        public void Alterar(VeiculoDTO veiculo)
        {
            if (veiculo.placa == string.Empty)
            {
                throw new ArgumentException("Placa é obrigatória.");
            }
            if (veiculo.ano == string.Empty)
            {
                throw new ArgumentException("Ano é obrigatório.");
            }
            if (veiculo.cor == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatória.");
            }
            db.Alterar(veiculo);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<VeiculoDTO> Consultar(string modelo)
        {
            return db.Consultar(modelo);

        }

        public List<VeiculoDTO> Listar()
        {
            return db.Listar();
        }
        
    }
}
