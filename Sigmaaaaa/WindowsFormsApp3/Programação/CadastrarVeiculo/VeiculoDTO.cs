﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.CadastrarVeiculo
{
 public class VeiculoDTO
    {
        public int idveiculo { get; set; }

        public string placa { get; set; }

        public string combustivel { get; set; }

        public string odometro { get; set; }

        public string cor { get; set; }

        public string ano { get; set; }

        public string proprietario { get; set; }

        public string modelo { get; set; }

        public int idcliente { get; set; }

        public string Informação { get; set; }
    }
}
