﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.ClienteJuridico
{
  public  class ClienteJuridicoDatabase
    {
        public int Salvar(ClienteJuridicoDTO clientejur)
        {
            string script =
                @"INSERT INTO tb_clientejuridico
                (
                   id_clientejuridico,
                   id_endereco,
                   ds_razaosocialjuridico,
                   ds_cnpjjuridico,
                   ds_atividadejuridico,
                   dt_nascimentojuridico,
                    ds_telefonejuridico,
                    ds_celularjuridico,
                   ds_telcomercialjuridico,
                    ds_emailjuridico,
                   ds_ramaljuridico,
                    ds_clienteobs
                )
                VALUES
                (
                   @id_clientejuridico,
                   @id_endereco,
                   @ds_razaosocialjuridico,
                   @ds_cnpjjuridico,
                   @ds_atividadejuridico,
                   @dt_nascimentojuridico,
                   @ds_telefonejuridico,
                   @ds_celularjuridico,
                   @ds_telcomercialjuridico,
                   @ds_emailjuridico,
                   @ds_ramaljuridico,
                   @ds_clienteobs)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_clientejuridico", clientejur.id_clientejuridico));
            parms.Add(new MySqlParameter("id_endereco", clientejur.id_endereco));
            parms.Add(new MySqlParameter("ds_razaosocialjuridico", clientejur.razaosocialjuridico));
            parms.Add(new MySqlParameter("ds_cnpjjuridico", clientejur.cnpjjuridico));
            parms.Add(new MySqlParameter("ds_atividadejuridico", clientejur.atividadejuridico));
            parms.Add(new MySqlParameter("dt_nascimentojuridico", clientejur.nascimentojuridico));
            parms.Add(new MySqlParameter("ds_telefonejuridico", clientejur.telefonejuridico));
            parms.Add(new MySqlParameter("ds_celularjuridico", clientejur.celularjuridico));
            parms.Add(new MySqlParameter("ds_telcomercialjuridico", clientejur.telcomercialjuridico));
            parms.Add(new MySqlParameter("ds_emailjuridico", clientejur.emailjuridico));
            parms.Add(new MySqlParameter("ds_ramaljuridico", clientejur.ramaljuridico));
            parms.Add(new MySqlParameter("ds_clienteobs", clientejur.clienteobsjuridico));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ClienteJuridicoDTO clientejur)
        {
            string script =
            @"UPDATE tb_clientejuridico
                SET 
                    id_clientejuridico = @id_clientejuridico,
                    id_endereco=@id_endereco,
	              ds_razaosocialjuridico = @ds_razaosocialjuridico,
	              ds_atividadejuridico = @ds_atividadejuridico,
	               dt_nascimentojuridico = @dt_nascimentojuridico,
	               ds_telefonejuridico = @ds_telefonejuridico,
	                ds_celularjuridico= @ds_celularjuridico,
                   ds_telcomercialjuridico = @ds_telcomercialjuridico,
                   ds_emailjuridico = @ds_emailjuridico,
                    ds_ramaljuridico= @ds_ramaljuridico,
                   ds_clienteobs= @ds_clienteobs
	                 WHERE id_clientejuridico = @ id_clientejuridico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_clientejuridico", clientejur.id_clientejuridico));
            parms.Add(new MySqlParameter("id_endereco", clientejur.id_endereco));
            parms.Add(new MySqlParameter("ds_razaosocialjuridico", clientejur.razaosocialjuridico));
            parms.Add(new MySqlParameter("ds_atividadejuridico", clientejur.atividadejuridico));
            parms.Add(new MySqlParameter("dt_nascimentojuridico", clientejur.nascimentojuridico));
            parms.Add(new MySqlParameter("ds_telefonejuridico", clientejur.telefonejuridico));
            parms.Add(new MySqlParameter(" ds_celularjuridico", clientejur.celularjuridico));
            parms.Add(new MySqlParameter("ds_telcomercialjuridico", clientejur.telcomercialjuridico));
            parms.Add(new MySqlParameter("ds_emailjuridico", clientejur.emailjuridico));
            parms.Add(new MySqlParameter("ds_ramaljuridico", clientejur.ramaljuridico));
            parms.Add(new MySqlParameter(" ds_clienteobs", clientejur.clienteobsjuridico));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_clientejuridico WHEREtb_clientejuridico= @tb_clientejuridico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("tb_clientejuridico", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

    }
}
