﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.ClienteJuridico
{
   public class ClienteJuridicoDTO
    {
        public int id_clientejuridico { get; set; }
        public int id_endereco{ get; set; }
        public string razaosocialjuridico { get; set; }
        public string cnpjjuridico{ get; set; }
        public string atividadejuridico { get; set; }
        public DateTime nascimentojuridico{ get; set; }
        public string telefonejuridico{ get; set; }
        public string celularjuridico{ get; set; }
        public string telcomercialjuridico{ get; set; }
        public string emailjuridico { get; set; }
        public string ramaljuridico{ get; set; }
        public string clienteobsjuridico{ get; set; }
    }
}
