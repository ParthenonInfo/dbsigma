﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.ClienteJuridico
{
    public class ClienteJuridicoBusiness
    {
        ClienteJuridicoDatabase db = new ClienteJuridicoDatabase();

        public int Salvar(ClienteJuridicoDTO clientejur)
        {
            if (clientejur.razaosocialjuridico == string.Empty)
            {
                throw new ArgumentException("Razão Social é obrigatório.");
            }
            if (clientejur.telcomercialjuridico== string.Empty)
            {
                throw new ArgumentException("telefone comecial é obrigatório.");
            }
            if (clientejur.telefonejuridico== string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (clientejur.cnpjjuridico== string.Empty)
            {
                throw new ArgumentException("cpj é obrigatório.");
            }
            if (clientejur.celularjuridico== string.Empty)
            {
                throw new ArgumentException("celular é obrigatório.");
            }

            if (clientejur.nascimentojuridico> DateTime.Now)
            {
                throw new ArgumentException("Data não valida");
            }

            return db.Salvar(clientejur);

        }

        public void Alterar(ClienteJuridicoDTO clientejur)
        {
            if (clientejur.razaosocialjuridico == string.Empty)
            {
                throw new ArgumentException("Razão Social é obrigatório.");
            }
            if (clientejur.telcomercialjuridico == string.Empty)
            {
                throw new ArgumentException("telefone comecial é obrigatório.");
            }
            if (clientejur.telefonejuridico == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (clientejur.cnpjjuridico == string.Empty)
            {
                throw new ArgumentException("cpj é obrigatório.");
            }
            if (clientejur.celularjuridico == string.Empty)
            {
                throw new ArgumentException("celular é obrigatório.");
            }

            if (clientejur.nascimentojuridico > DateTime.Now)
            {
                throw new ArgumentException("Data não valida");
            }

            if (clientejur.nascimentojuridico.Year > 1999)
            {
                throw new ArgumentException("tem que ser maior de 18");
            }
            db.Alterar(clientejur);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

    }
}
