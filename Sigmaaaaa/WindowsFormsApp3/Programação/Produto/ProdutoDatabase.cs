﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Produto
{
   public class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO produto)
        {
            string script =
                @"INSERT INTO tb_produto

                (
                    id_produto,
                    nm_produto,
                    vl_preco,
                    ds_descricao
                )
                VALUES
                (
                    @id_produto,
                    @nm_produto,
                    @vl_preco,
                    @ds_descricao
                   
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", produto.id));
            parms.Add(new MySqlParameter("nm_produto", produto.nomeproduto));
            parms.Add(new MySqlParameter("vl_preco", produto.preco));
            parms.Add(new MySqlParameter("ds_descricao", produto.descricao));
          
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ProdutoDTO produto)
        {
            string script =
            @"UPDATE tb_produto
                 SET id_produto  = @id_produto,
                  nm_produto= @nm_produto,
                  vl_preco = @vl_preco,
                  ds_descricao= @ds_descricao,
                  WHERE id_produto = @idVeiculo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", produto.id));
            parms.Add(new MySqlParameter("nm_produto", produto.nomeproduto));
            parms.Add(new MySqlParameter("vl_preco", produto.preco));
            parms.Add(new MySqlParameter("ds_descricao",produto.descricao));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {

            string script =
                @"SELECT * FROM tb_produto
                  WHERE nm_produto like @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + produto + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> produtos = new List<ProdutoDTO>();
            while (reader.Read())
            {
               
                    ProdutoDTO novoproduto = new ProdutoDTO();
                    novoproduto.id = reader.GetInt32("id_produto");
                    novoproduto.nomeproduto = reader.GetString("nm_produto");
                    novoproduto.preco = reader.GetDecimal("vl_preco");
                    novoproduto.descricao = reader.GetString("ds_descricao");

                    produtos.Add(novoproduto);
                
               
            }
            return produtos;


        }

        public List<ProdutoDTO> Listar()
        {
            string script =
                @"SELECT * FROM tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> produtos = new List<ProdutoDTO>();
            while (reader.Read())
            {

                ProdutoDTO novoproduto = new ProdutoDTO();
                novoproduto.id = reader.GetInt32("id_produto");
                novoproduto.nomeproduto = reader.GetString("nm_produto");
                novoproduto.preco = reader.GetDecimal("vl_preco");
                novoproduto.descricao = reader.GetString("ds_descricao");

                produtos.Add(novoproduto);


            }
            return produtos;

        }


    }

}

