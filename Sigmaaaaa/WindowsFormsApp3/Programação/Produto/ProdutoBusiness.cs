﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Produto
{
  public class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();
        public int Salvar(ProdutoDTO produto)
        {
            if (produto.nomeproduto == string.Empty)
            {
                throw new ArgumentException("Nome do Produto é obrigatório.");
            }
            if (produto.preco < 1)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }
     
            
            return db.Salvar(produto);
        }

        public void Alterar(ProdutoDTO produto)
        {
            if (produto.nomeproduto == string.Empty)
            {
                throw new ArgumentException("Nome do Produto é obrigatório.");
            }
            if (produto.preco < 1)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }
            db.Alterar(produto);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            if (produto == string.Empty)
            {
                produto = string.Empty;
            }

            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            return db.Listar();
        }
        

    }
}
