﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Orçamento
{
 public   class OrcamentoProdutoitemDatabase
    {
        public int Salvar(OrcamentoProdutoitemDTO orcamentos)
        {
            string script =
                @"INSERT INTO tb_orcamentoprodutoitem
                  (
                    id_orcamentoprodutoitem,
                    id_produto,
                    id_orcamento
                  )
                  VALUES
                  (
                    @id_orcamentoprodutoitem,
                    @id_produto,
                    @id_orcamento
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_orcamentoprodutoitem", orcamentos.id_orcamentoprodutoitem));
            parms.Add(new MySqlParameter("id_produto", orcamentos.id_produto));
            parms.Add(new MySqlParameter("id_orcamento", orcamentos.id_orcamento));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
    }
}
