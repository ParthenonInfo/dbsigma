﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Orçamento
{
   class OrcamentoItemDatabase
    {
     public   int Salvar(OrcamentoItemDTO orçamento)
        {
            string script =
                @"INSERT INTO tb_orcamento
                  (
                    id_orcamento,
                    id_veiculo,
                    dt_orcamento,
                    ds_falha
                  )
                  VALUES
                  (
                    @id_orcamento,
                    @id_veiculo,
                    @dt_orcamento,
                    @ds_falha
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_orcamento", orçamento.idorcamento));
            parms.Add(new MySqlParameter("dt_orcamento", orçamento.dataorcamento));
            parms.Add(new MySqlParameter("id_veiculo", orçamento.idveiculo));
            parms.Add(new MySqlParameter("ds_falha", orçamento.falha));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
    }
}
