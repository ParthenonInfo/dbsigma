﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Produto;
using WindowsFormsApp3.Programação.Serviço;

namespace WindowsFormsApp3.Programação.Orçamento
{
    class OrcamentoItemBusiness
    {
        OrcamentoItemDatabase db = new OrcamentoItemDatabase();

        public int Salvar(OrcamentoItemDTO orcamento, List<ProdutoDTO> orcamentos, List<ServiçoDTO> orcamentoo)
        {
            if(orcamento.falha == string.Empty)
            {
                throw new ArgumentException("Digite a falha");
            }

            int id_orcamento = db.Salvar(orcamento);

            OrcamentoProdutoitemBusinesscs itemBusiness = new OrcamentoProdutoitemBusinesscs();
            foreach (ProdutoDTO item in orcamentos)
            {
                OrcamentoProdutoitemDTO produtoitem = new OrcamentoProdutoitemDTO();
                produtoitem.id_orcamento = id_orcamento;
                produtoitem.id_produto=item.id;

                itemBusiness.Salvar(produtoitem); 
            }
            OrcamentoServicoItemBusiness servicobusiness = new OrcamentoServicoItemBusiness();
            foreach (ServiçoDTO item in orcamentoo)
            {
                OrcamentoServicoItemDTO servicoitem = new OrcamentoServicoItemDTO();
                servicoitem.id_orcamento = id_orcamento;
                servicoitem.id_servico = item.id;
                servicobusiness.Salvar(servicoitem);
            }



            return db.Salvar(orcamento);
        }

    }
}
