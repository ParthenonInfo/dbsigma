﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Orçamento
{
   public class OrcamentoServicoItemDatabase
    {
        public int Salvar(OrcamentoServicoItemDTO orcamentoo)
        {
            string script =
                @"INSERT INTO tb_orcamentoservicoitem
                  (
                   id_orcamentoservicoitem,
                   id_servico,
                    id_orcamento
                  )
                  VALUES
                  (
                    @id_orcamentoservicoitem,
                    @id_servico,
                    @id_orcamento
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_orcamentoservicoitem", orcamentoo.id_orcamentoservicoitem));
            parms.Add(new MySqlParameter("id_servico", orcamentoo.id_servico));
            parms.Add(new MySqlParameter("id_orcamento", orcamentoo.id_orcamento));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
    }
}
