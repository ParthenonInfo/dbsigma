﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Orçamento
{
  public  class OrcamentoItemDTO
    {
        public int idorcamento { get; set; }

        public int idveiculo { get; set; }

        public string falha { get; set; }

        public DateTime dataorcamento { get; set; }

    }
}
