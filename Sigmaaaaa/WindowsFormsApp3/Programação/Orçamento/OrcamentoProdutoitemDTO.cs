﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3.Programação.Orçamento
{
   public class OrcamentoProdutoitemDTO
    {
        public int id_orcamentoprodutoitem { get; set; }
        public int id_produto { get; set; }
        public int id_orcamento { get; set; }
    }
}
